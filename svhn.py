import numpy as np
import scipy.io as sio
from keras.preprocessing.image import ImageDataGenerator

from keras import backend as K
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Activation, Conv2D, MaxPooling2D, Flatten, Dropout, BatchNormalization
from sklearn.model_selection import train_test_split

train_data = sio.loadmat('/gpfs/home/nct01/nct01011/codes/Assignment1/data/train_32x32.mat')
test_data = sio.loadmat('/gpfs/home/nct01/nct01011/codes/Assignment1/data/test_32x32.mat')

# access to the dict
x_train = train_data['X']
y_train = train_data['y']

x_test = test_data['X']
y_test = test_data['y']

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train = x_train / 255
x_test = x_test / 255

y_train = np.reshape(y_train, y_train.shape[0])
y_test = np.reshape(y_test, y_test.shape[0])
y_train.flatten()
y_test.flatten()

for i in range(0, y_train.shape[0]):
    y_train[i] %= 10

for i in range(0, y_test.shape[0]):
    y_test[i] %= 10

# Adapt the labels to the one-hot vector syntax required by the softmax

y_train = np_utils.to_categorical(y_train, 10)
y_test = np_utils.to_categorical(y_test, 10)

# image resolution
img_rows, img_cols = 32, 32

# Find which format to use (depends on the backend), and compute input_shape
if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[3], 3, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[3], 3, img_rows, img_cols)
    input_shape = (3, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[3], img_rows, img_cols, 3)
    x_test = x_test.reshape(x_test.shape[3], img_rows, img_cols, 3)
    input_shape = (img_rows, img_cols, 3)

# Define the NN architecture

# Two hidden layers
nn = Sequential()
nn.add(Conv2D(32, 3, 3, input_shape=input_shape))
nn.add(BatchNormalization())
nn.add(Activation('relu'))
nn.add(Conv2D(32, 3, 3, input_shape=input_shape))
nn.add(BatchNormalization())
nn.add(Activation('relu'))
nn.add(MaxPooling2D(pool_size=(2, 2)))

nn.add(Conv2D(64, 3, 3))
nn.add(BatchNormalization())
nn.add(Activation('relu'))
nn.add(Conv2D(64, 3, 3))
nn.add(BatchNormalization())
nn.add(Activation('relu'))
nn.add(MaxPooling2D(pool_size=(2, 2)))

nn.add(Flatten())
nn.add(Dense(512, activation='relu'))
nn.add(Dense(10, activation='softmax'))


# Compile the NN
nn.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])


imageGen = ImageDataGenerator(
    rotation_range=15,
    width_shift_range=0.1,
    height_shift_range=0.1,
    horizontal_flip=True)


# training the image preprocessing
imageGen.fit(x_train)

x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.15)

batchSize = 32
epochs = 100

validation_datagen = ImageDataGenerator()
validation_generator = validation_datagen.flow(x_validation, y_validation, batch_size=batchSize)

# fits the model on batches with real-time data augmentation
history = nn.fit_generator(imageGen.flow(x_train, y_train, batch_size=batchSize),
                    steps_per_epoch=len(x_train) / batchSize, epochs=epochs,
                           validation_data=validation_generator,
                           validation_steps=len(x_validation) / batchSize)
# Evaluate the model with test set
score = nn.evaluate(x_test, y_test, verbose=0)
print('test loss:', score[0])
print('test accuracy:', score[1])

##Store Plots
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

# Accuracy plot
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
# No validation loss in this example
plt.legend(['train', 'val'], loc='upper left')
plt.savefig('model_accuracy.pdf')
plt.close()
# Loss plot
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.savefig('model_loss.pdf')