from __future__ import division
import keras
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras import backend as K
from keras.datasets import cifar100
from keras.models import Sequential
from keras.layers import Dense, Activation, Conv2D, MaxPooling2D, Flatten, Dropout, BatchNormalization
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping


print('Using Keras version', keras.__version__)

# Load the cifar dataset, already provided by Keras
(x_train, y_train), (x_test, y_test) = cifar100.load_data()

# Check sizes of dataset
print('Number of train examples', x_train.shape[0])
print('Size of train examples', x_train.shape[1:])

# Normalize data
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train = x_train / 255
x_test = x_test / 255

# Adapt the labels to the one-hot vector syntax required by the softmax


y_train = np_utils.to_categorical(y_train, 100)
y_test = np_utils.to_categorical(y_test, 100)
y_train = y_train[:, 0, :]
y_test = y_test[:, 0, :]

# resolution
img_rows, img_cols = 32, 32

# Find which format to use (depends on the backend), and compute input_shape
if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 3, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 3, img_rows, img_cols)
    input_shape = (3, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 3)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 3)
    input_shape = (img_rows, img_cols, 3)

# Define the NN architecture
nn = Sequential()

nn.add(Conv2D(32, 3, 3, input_shape=input_shape))
nn.add(BatchNormalization())
nn.add(Activation('relu'))
nn.add(Conv2D(32, 3, 3, input_shape=input_shape))
nn.add(BatchNormalization())
nn.add(Activation('relu'))
nn.add(MaxPooling2D(pool_size=(2, 2)))
# nn.add(Dropout(0.25))

nn.add(Conv2D(64, 3, 3))
nn.add(BatchNormalization())
nn.add(Activation('relu'))
nn.add(Conv2D(64, 3, 3))
nn.add(BatchNormalization())
nn.add(Activation('relu'))
nn.add(MaxPooling2D(pool_size=(2, 2)))
# nn.add(Dropout(0.25))

nn.add(Flatten())
nn.add(Dense(512, activation='relu'))
# nn.add(Dropout(0.5))
nn.add(Dense(100, activation='softmax'))

# Model visualization
# We can plot the model by using the ```plot_model``` function. We need to install *pydot, graphviz and pydot-ng*
from keras.utils import plot_model
plot_model(nn, to_file='nn.png', show_shapes="true")

# Compile the NN
nn.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])

imageGen = ImageDataGenerator(
    rotation_range=15,
    width_shift_range=0.1,
    height_shift_range=0.1,
    horizontal_flip=True)

# training the image preprocessing
imageGen.fit(x_train)

x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.15)

batchSize = 32
epochs = 100

validation_datagen = ImageDataGenerator()
validation_generator = validation_datagen.flow(x_validation, y_validation, batch_size=batchSize)

earlystop = EarlyStopping(monitor='val_acc', min_delta=0.0001, patience=5, verbose=1, mode='auto')

# fits the model on batches with real-time data augmentation:
history = nn.fit_generator(imageGen.flow(x_train, y_train, batch_size=batchSize),
                           steps_per_epoch=len(x_train) / batchSize, epochs=epochs,
                           validation_data=validation_generator,
                           validation_steps=len(x_validation) / batchSize,
                           callbacks=[earlystop])


# Evaluate the model with test set
score = nn.evaluate(x_test, y_test, verbose=0)
print('test loss:', score[0])
print('test accuracy:', score[1])

##Store Plots
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

# Accuracy plot
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
# No validation loss in this example
plt.legend(['train', 'val'], loc='upper left')
plt.savefig('model_accuracy.pdf')
plt.close()
# Loss plot
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.savefig('model_loss.pdf')
